<?php

use Framework\Http\Core;
use Framework\Support\Environment;


// autoload, созданный композером
require __DIR__ . '/../vendor/autoload.php';

$env = require './../application/env.php';
Environment::setBatch($env);

$routes = require './../application/routes.php';

echo (new Core($routes))->response();
