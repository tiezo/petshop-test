-- Adminer 4.3.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `AllowedTables`;
CREATE TABLE `AllowedTables` (
  `TableName` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `AllowedTables` (`TableName`) VALUES
('Session'),
('User');

DROP TABLE IF EXISTS `Session`;
CREATE TABLE `Session` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `TimeOfEvent` datetime NOT NULL,
  `Description` text NOT NULL,
  `SpeakersLimit` int(10) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `Session` (`ID`, `Name`, `TimeOfEvent`, `Description`, `SpeakersLimit`) VALUES
(1,	'fsdf',	'2017-07-04 10:17:19',	'sdfsf',	4);

DROP TABLE IF EXISTS `SessionSpeakers`;
CREATE TABLE `SessionSpeakers` (
  `UserId` int(11) NOT NULL,
  `SessionId` int(11) NOT NULL,
  PRIMARY KEY (`UserId`,`SessionId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `SessionSpeakers` (`UserId`, `SessionId`) VALUES
(1,	1),
(2,	1);

DROP TABLE IF EXISTS `User`;
CREATE TABLE `User` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Email` varchar(255) NOT NULL,
  `Name` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `User` (`ID`, `Email`, `Name`) VALUES
(1,	'airmail@code-pilots.com',	'The first user');

-- 2017-07-04 14:15:59
