# тестовое задание petshop

## что было сделано:

1. написан мини-фреймворк (папка framework - mvc, роутинг, query builder и прочее)
2. реализованы методы /Table и /SessionSubscribe, обработка ошибок
3. в /SessionSubcribe атомарность достигается с помощью проведения запроса в транзакции. запрос на добавление имеет примерно такой вид. как видно, вставка не выполнится, 
если превышено кол-во спикеров сессии

```SQL
INSERT INTO SessionSpeakers(UserId, SessionId) 
SELECT 3, 1 
WHERE NOT EXISTS(
    SELECT COUNT(UserId) as CNT, Session.SpeakersLimit  
    FROM SessionSpeakers 
    JOIN Session on SessionSpeakers.SessionId = Session.ID 
    WHERE SessionId = 1 
    HAVING CNT >= Session.SpeakersLimit
)
```

## что не успел:

в /Table пока поддерживается только ManyToMany, думаю, для демо этого достаточно. OneToMany и прочие связи не успел, извините

## как тестировать:

1. залить dump.sql в свою БД
2. в конфиге application/env.php прописать данные базы
3. сделать composer install (установится лишь autoload путей по psr-4)
4. конфиг nginx-a есть в корне (foo.app.conf)
5. тестировать можно, как с помощью POST, так и GET (сделано для удобства тестирование, никаких csrf-токенов нет)