<?php

namespace Framework\Http\Request;


class Request
{
    protected $parameters;

    public function __construct()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            list($uri, $query) = explode('?', $_SERVER['REQUEST_URI']);

            foreach (explode('&', $query) as $pair) {
                list($key, $value) = explode('=', $pair);
                $this->parameters[$key] = $value;
            }
        }

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (isset($_SERVER['HTTP_CONTENT_TYPE']) && $_SERVER['HTTP_CONTENT_TYPE'] === 'application/json') {
                try {
                    $this->parameters = json_encode(file_get_contents('php://input'));
                } catch (\Exception $e) {
                    // encoding went wrong
                    $this->parameters = [];
                }

                return;
            }

            $this->parameters = $_REQUEST;

            return;
        }
    }

    public function get($parameter)
    {
        if (!isset($this->parameters[$parameter])) {
            return null;
        }

        return $this->parameters[$parameter];
    }
}