<?php

namespace Framework\Http\Response;

abstract class AbstractResponse
{
    /**
     * @var array
     */
    protected $headers;

    protected $content;

    abstract public function serialize();

    public function __construct($content)
    {
        $this->setContent($content);
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @param $headers
     */
    public function setHeaders($headers)
    {
        $this->headers = $headers;
    }

    /**
     * @return mixed
     */
    public function getHeaders()
    {
        return $this->headers;
    }
}