<?php

namespace Framework\Http\Response;

class JsonResponse extends AbstractResponse
{
    protected $headers = [
        'Content-Type' => 'application/json'
    ];

    /**
     * @return mixed
     */
    public function serialize()
    {
        return json_encode($this->getContent());
    }
}