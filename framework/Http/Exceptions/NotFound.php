<?php

namespace Framework\Http\Exceptions;


class NotFound extends HttpException
{
    public function __construct()
    {
        parent::__construct(404, 'Not Found');
    }
}