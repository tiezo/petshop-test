<?php

namespace Framework\Http\Exceptions;


class HttpException extends \Exception
{
    /**
     * @var int
     */
    protected $code;

    /**
     * @var string
     */
    protected $message;

    public function __construct($code, $message)
    {
        $this->code = $code;
        $this->message = $message;
    }
}