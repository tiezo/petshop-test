<?php

namespace Framework\Http;

use Framework\Http\Exceptions\NotFound;
use Framework\Http\Response\AbstractResponse;

/**
 * Parses routes and calls method of associated controller
 */
class Router
{
    /**
     * @var array
     */
    protected $routes;

    /**
     * @param array $routes
     * @throws NotFound
     */
    public function __construct($routes)
    {
        $this->routes = $routes;
        $this->normalizeRoutes();
    }

    /**
     * приводит все ключи к нижнему регистру. костыль
     */
    private function normalizeRoutes()
    {
        $this->routes = array_flip(array_map(function ($v) {
            return mb_strtolower($v);
        }, array_flip($this->routes)));
    }

    /**
     * @return AbstractResponse
     * @throws NotFound
     */
    public function handle()
    {
        $uri = $this->getCurrentUri();

        $controllerMapping = $this->resolveController($uri);

        if (is_null($controllerMapping)) {
            throw new NotFound();
        }

        list($resolvedController, $resolvedControllerMethod) = explode('@', $controllerMapping);

        return call_user_func_array([new $resolvedController(), $resolvedControllerMethod], func_get_args());
    }

    /**
     * ищем по ключу в роутинге наш uri
     * @param $uri
     * @return string
     */
    public function resolveController($uri)
    {
        return isset($this->routes[$uri]) ? $this->routes[$uri]: null;
    }

    /**
     * @return string
     */
    public function getCurrentUri()
    {
        $parts = explode('?', mb_strtolower($_SERVER['REQUEST_URI']));
        return $parts[0];
    }
}