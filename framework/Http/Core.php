<?php

namespace Framework\Http;


use Framework\Http\Exceptions\HttpException;

class Core
{
    /**
     * @var array
     */
    private $routes;

    public function __construct($routes)
    {
        $this->routes = $routes;
    }

    /**
     * @return mixed
     */
    public function response()
    {
        try {
            $router = new Router($this->routes);
            $response = $router->handle();

            foreach ($response->getHeaders() as $header => $value) {
                header("$header: $value");
            }

            return $response->serialize();
        } catch (HttpException $e) {
            header("HTTP/1.1 {$e->getCode()} {$e->getMessage()}");

            return (new \Framework\Http\Response\JsonResponse([
                'error' => $e->getCode(), 'message' => $e->getMessage()
            ]))->serialize();
        }
    }
}