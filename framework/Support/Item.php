<?php

namespace Framework\Support;

/**
 * враппер над хэшмапой
 * @package Framework\Support
 */
class Item extends ArrayLike
{
    public $data;

    public function __construct($item)
    {
        $this->data = $item;
    }

    public function getByKey($key)
    {
        return $this->data[$key];
    }
}