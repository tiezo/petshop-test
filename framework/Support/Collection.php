<?php

namespace Framework\Support;


/**
 * Список хэшмап
 * @package Framework\Support
 */
class Collection extends ArrayLike
{
    public $data;

    /**
     * Collection constructor.
     * @param array $collection
     */
    public function __construct($collection)
    {
        $this->data = $collection;
    }

    public function getByKey($key)
    {
        return array_map(function ($item) use ($key) {
            return $item->{$key};
        }, $this->data);
    }
}