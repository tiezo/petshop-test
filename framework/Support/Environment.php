<?php

namespace Framework\Support;

/**
 * Wrapper on putenv() and getenv()
 */
class Environment
{
    /**
     * @param $variable
     * @param string $default
     * @return array|false|string
     */
    static public function get($variable, $default = '')
    {
        $value = getenv($variable);

        if (!$value) {
            return $default;
        }

        return $value;
    }

    /**
     * @param $name
     * @param $value
     */
    static public function set($name, $value)
    {
        putenv("$name=$value");
    }

    /**
     * @param $variables
     */
    static public function setBatch($variables)
    {
        foreach ($variables as $name => $value) {
            self::set($name, $value);
        }
    }
}