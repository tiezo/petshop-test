<?php

namespace Framework\Database;


use Framework\Database\Query\QueryBuilder;
use Framework\Support\ArrayLike;
use Framework\Support\Collection;

class Model
{
    /**
     * @var string
     */
    protected $table;

    /**
     * primary key
     *
     * @var string
     */
    static public $key;

    /**
     * @var array
     */
    protected $relations;

    /**
     * @var array
     */
    public $data;

    /**
     * @param $data
     */
    public function __construct(ArrayLike $data = null)
    {
        $this->data = $data;

        // TODO: polymorphic
        $this->relations = [
            'oneToOne' => [],
            'oneToMany' => [],
            'manyToMany' => []
        ];
    }


    public function loadRelations()
    {
        $this->loadManyToMany();
        $this->loadOneToMany();
        $this->manyToMany();
    }

    protected function loadManyToMany()
    {
        foreach ($this->relations['manyToMany'] as $relation) {
            list($primaryKey, $relationKey) = $relation['keys'];

            $keys = (new QueryBuilder())
                ->select([$relationKey])
                ->from($relation['mediate'])
                ->rawWhere(sprintf($relationKey .' IN (%s)', implode(', ', $this->data->getByKey($primaryKey))))
                ->build()->getQueryString();

            /** @var Model $className */
            $className = '\\Application\\Models\\' . $relation['to'];

            $key = $className::$key;

            $query = (new QueryBuilder())
                ->select(['*'])
                ->from($relation['to'])
                ->rawWhere(sprintf("$key IN (%s)", $keys))
                ->build();

            $result = Connection::performQuery($query)->all();

            if ($this->data instanceof Collection) {
                // все лучше, чем n^2
                $map = [];

                foreach ($result as $item) {
                    $map[$item->ID] = $item;
                }


                for ($i = 0; $i < count($this->data->data); $i++) {
                    $this->data->data[$i]->{$relation['name']}= $map[$this->data->data[$i]->ID];
                }
            }
        }
    }

    protected function loadOneToMany()
    {

    }

    protected function manyToMany()
    {

    }
}