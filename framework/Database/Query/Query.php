<?php


namespace Framework\Database\Query;


class Query
{
    /**
     * @var string
     */
    private $queryString;

    /**
     * @var array
     */
    private $bindings;

    /**
     * Query constructor.
     * @param $queryString
     * @param array $bindings
     */
    public function __construct($queryString, $bindings = [])
    {
        $this->queryString = $queryString;
        $this->bindings = $bindings;
    }

    /**
     * @return string
     */
    public function getQueryString()
    {
        return $this->queryString;
    }

    /**
     * @return array
     */
    public function getBindings()
    {
        return $this->bindings;
    }
}