<?php

namespace Framework\Database\Query;
use Couchbase\Bucket;


/**
 * очень упрощенный querybuilder, много чего не поддерживает (group by, order by, etc)
 *
 * @package Framework\Database
 */
class QueryBuilder
{
    /**
     * SQL query
     *
     * @var string
     */
    private $queryString;

    /**
     * PDO bindings (['field' => ':field'])
     *
     * @var array
     */
    private $bindings;

    /**
     * tokens
     */
    private $columns;
    private $whereClauses;
    private $rawWhere;
    private $joins;
    private $table;
    private $from;
    private $values;
    private $having;

    /**
     * type of query (SELECT, INSERT, UPDATE, DELETE)
     * @var string
     */
    private $queryType;

    /**
     * @param string $from
     * @return $this
     * @throws \Exception
     */
    public function from($from)
    {
        $this->from = $from;
        return $this;
    }

    /**
     * @param $columnsOrAggregates
     * @return $this
     */
    public function select($columnsOrAggregates)
    {
        $this->queryType = 'Select';

        $this->columns = $columnsOrAggregates;
        return $this;
    }

    /**
     * @param string $table
     */
    public function update($table = '')
    {
        $this->queryType = 'Update';
        $this->table = $table;
    }

    /**
     * @param $table
     * @param $columns
     * @param $values
     * @return $this
     */
    public function insert($table, $columns, $values)
    {
        $this->queryType = 'Insert';
        $this->table = $table;
        $this->columns = $columns;
        $this->values = $values;
        return $this;
    }

    /**
     * @param $from
     */
    public function delete($from)
    {
        $this->queryType = 'Delete';
        $this->table = $from;
    }

    /**
     * @param $field
     * @param $operator
     * @param $value
     * @param string $condition
     * @return $this
     */
    public function where($field, $operator, $value, $condition = 'WHERE')
    {
        $this->whereClauses[] = [$condition, $field, $operator, $value];
        return $this;
    }

    /**
     * @param QueryBuilder $builder
     */
    public function whereNotExists(QueryBuilder $builder)
    {
        $this->rawWhere = ' WHERE NOT EXISTS(' . $builder->build()->getQueryString() . ')';
    }

    /**
     * @param $field
     * @param $operator
     * @param $value
     * @return $this
     */
    public function andWhere($field, $operator, $value)
    {
        $this->where($field, $operator, $value, 'AND');

        return $this;
    }

    /**
     * @param $field
     * @param $operator
     * @param $value
     * @return $this
     */
    public function orWhere($field, $operator, $value)
    {
        $this->where($field, $operator, $value, 'OR');

        return $this;
    }

    /**
     * @param $rawWhere
     * @return $this
     */
    public function rawWhere($rawWhere)
    {
        $this->rawWhere = ' WHERE ' . $rawWhere;
        return $this;
    }

    /**
     * @param $field
     * @param $operator
     * @param $compare
     * @return $this
     */
    public function having($field, $operator, $compare)
    {
        // TODO: додлеать

        $this->having = " HAVING $field $operator $compare";
        return $this;
    }

    /**
     * @param $table
     * @return $this
     */
    public function join($table)
    {
        $this->joins[] = ' JOIN ' . $table;
        return $this;
    }

    /**
     * @return static
     */
    public function newBuilder()
    {
        return new static();
    }

    /**
     * Общий метод построения
     * @return Query
     */
    public function build()
    {
        $method = "build{$this->queryType}";
        $this->$method();

        return new Query($this->queryString, $this->bindings);
    }

    /**
     * Строит select
     * @return void
     */
    public function buildSelect()
    {
        $this->queryString = sprintf('SELECT %s ', implode(', ', $this->columns));

        if (mb_strlen($this->from)) {
            $this->queryString .= " FROM {$this->from} ";
        }

        if (!empty($this->joins)) {
            $this->queryString .= implode(' ', $this->joins);
        }

        if (!empty($this->whereClauses)) {
            $this->queryString .= $this->compilePreparedWhereClauses();
        }

        if (mb_strlen($this->rawWhere)) {
            $this->queryString .= $this->rawWhere;
        }

        if (mb_strlen($this->having)) {
            $this->queryString .= $this->having;
        }
    }

    /**
     * Строит запрос insert into. Если в $this->values QueryBuilder, строит и его
     * @return void
     */
    public function buildInsert()
    {
        if (is_array($this->values)) {
            for ($i = 0; $i < count($this->columns); $i++) {
                $this->prepareField($this->columns[$i], $this->values);
            }

            $this->queryString = sprintf(
                'INSERT INTO %s(%s) VALUES(%s)',
                $this->table, implode(', ', $this->columns), implode(', ', $this->values)
            );

            return;
        }

        if ($this->values instanceof QueryBuilder) {
            $this->queryString = sprintf(
                'INSERT INTO %s(%s) %s',
                $this->table, implode(', ', $this->columns), $this->values->build()->getQueryString()
            );

            if ($this->rawWhere) {
                $this->queryString .= $this->rawWhere;
            }

            return;
        }
    }

    /**
     * @param $field
     * @param $value
     */
    public function prepareField($field, $value)
    {
        $this->bindings[':' . $field] = $value;
    }

    /**
     * Compile array of WHERE clauses to string with bindings
     *
     * @return string
     */
    private function compilePreparedWhereClauses()
    {
        $where = [];

        foreach ($this->whereClauses as $whereClause) {
            $this->prepareField($whereClause[1], $whereClause[3]);

            $clause = array_slice($whereClause, 0, 3);
            $clause[] = ":{$whereClause[1]}";

            $where[] = implode(' ', $clause);
        }

        return ' ' . implode(' ', $where) . ' ';
    }
}