<?php

namespace Framework\Database\Query;

/**
 * query result
 * @package Framework\Database
 */
class Result
{
    private $result;
    public $rowCount;

    /**
     * Result constructor.
     * @param array $result
     * @param int $rowCount
     */
    public function __construct($result = [], $rowCount = 0)
    {
        $this->result = $result;
        $this->rowCount = $rowCount;
    }

    /**
     * @return mixed|null
     */
    public function one()
    {
        if (isset($this->result[0])) {
            return $this->result[0];
        }

        return null;
    }

    /**
     * @return array
     */
    public function all()
    {
        return $this->result;
    }
}