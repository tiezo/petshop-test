<?php

namespace Framework\Database;


use Framework\Database\Exceptions\CommitFailedException;
use Framework\Database\Exceptions\TransactionFailedException;
use Framework\Database\Query\Query;
use Framework\Database\Query\Result;
use Framework\Support\Environment;
use PDO;

class Connection
{
    /**
     * @var PDO
     */
    static private $connection;

    private function __constructor()
    {
        // singleton
    }

    /**
     * @return PDO
     */
    static public function getInstance()
    {
        $driver = Environment::get('DB_DRIVER');
        $host = Environment::get('DB_HOST', 'localhost');
        $username = Environment::get('DB_USERNAME');
        $password = Environment::get('DB_PASSWORD');
        $db_name = Environment::get('DB_NAME');
        $charset = Environment::get('DB_CHARSET', 'utf8');
        $collation = Environment::get('DB_COLLATION', 'utf8_unicode_ci');

        if (is_null(self::$connection)) {
            $connection_string = sprintf(
                "%s:host=%s;dbname=%s;charset=%s;collation=%s",
                $driver, $host, $db_name, $charset, $collation
            );

            self::$connection = new PDO($connection_string, $username, $password);
        }

        return self::$connection;
    }

    /**
     * @param $query
     * @return Result
     * @throws CommitFailedException
     * @throws TransactionFailedException
     */
    static public function performQueryInTransaction(Query $query)
    {
        $connection = self::getInstance();

        if (!$connection->beginTransaction()) {
            $connection->rollBack();

            throw new TransactionFailedException();
        }

        $result = self::performQuery($query);



        if (!$connection->commit()) {
            throw new CommitFailedException();
        }

        return $result;
    }

    /**
     * @param $query
     * @return Result
     */
    static public function performQuery(Query $query)
    {
        $connection = self::getInstance();

        $preparedStatement = $connection->prepare($query->getQueryString());

        if ($bindings = $query->getBindings()) {
            foreach ($bindings as $parameter => $value) {
                $dataType = self::getPDODataType($value);

                $preparedStatement->bindValue($parameter, $value, $dataType);
            }
        }

        $preparedStatement->execute();

        return new Result($preparedStatement->fetchAll(PDO::FETCH_CLASS), $preparedStatement->rowCount());;
    }

    /**
     * @param $value
     * @return int
     */
    static private function getPDODataType($value)
    {
        if (is_numeric($value)) {
            // is_int() тут не поможет, делаем матчинг
            if (preg_match('/^[0-9]+$/', $value)) {
                return PDO::PARAM_INT;
            }
        }

        return PDO::PARAM_STR;
    }
}