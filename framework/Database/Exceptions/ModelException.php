<?php
/**
 * Created by PhpStorm.
 * User: kinoto
 * Date: 04.07.17
 * Time: 8:05
 */

namespace Framework\Database\Exceptions;


use Throwable;

class ModelException extends \Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}