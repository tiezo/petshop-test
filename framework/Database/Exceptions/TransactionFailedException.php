<?php

namespace Framework\Database\Exceptions;


use Throwable;

class TransactionFailedException extends \Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct('Transaction failed to begin', $code, $previous);
    }
}