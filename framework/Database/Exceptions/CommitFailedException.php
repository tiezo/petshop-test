<?php

namespace Framework\Database\Exceptions;


use Throwable;

class CommitFailedException extends \Exception
{
    public function __construct($message = '', $code = 0, Throwable $previous = null)
    {
        parent::__construct('Transaction commit failed', $code, $previous);
    }

}