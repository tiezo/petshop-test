<?php

return [
    '/Table' => 'Application\Controllers\TableController@table',
    '/SessionSubscribe' => 'Application\Controllers\SessionController@subscribe',
    '/' => 'Application\Controllers\IndexController@index'
];