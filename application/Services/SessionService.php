<?php

namespace Application\Services;


use Application\Models\User;
use Framework\Database\Connection;
use Framework\Database\Exceptions\ModelException;
use Framework\Database\Query\QueryBuilder;

class SessionService
{
    /**
     * @param $userId
     * @param $sessionId
     * @return bool
     */
    public function isSpeaker($userId, $sessionId)
    {
        $queryBuilder = new QueryBuilder();
        $queryBuilder
            ->select(['COUNT(UserId) as cnt'])
            ->from('SessionSpeakers')
            ->where('SessionId', '=', $sessionId)
            ->andWhere('UserId', '=', $userId);

        $result = Connection::performQuery($queryBuilder->build())->one();

        return !!$result->cnt;
    }

    /**
     * @param $userId
     * @param $sessionId
     * @return bool
     */
    public function attemptToSubscribe($userId, $sessionId)
    {
        $queryBuilder = new QueryBuilder();

        $queryBuilder
            ->insert(
                'SessionSpeakers',
                ['UserId', 'SessionId'],
                (new QueryBuilder())->select([$userId, $sessionId])
            )
            ->whereNotExists(
                (new QueryBuilder())
                    ->select(['COUNT(UserId) as CNT', 'Session.SpeakersLimit'])
                    ->from('SessionSpeakers')
                    ->join('Session on SessionSpeakers.SessionId = Session.ID')
                    ->rawWhere("SessionId = $sessionId")
                    ->having('CNT', '>=', 'Session.SpeakersLimit')
            );

        // делаем все это в транзакции
        $result = Connection::performQueryInTransaction($queryBuilder->build());

        return (bool) $result->rowCount;
    }

    /**
     * @param $userId
     * @param $sessionId
     * @return array
     * @throws ModelException
     */
    public function subscribe($userId, $sessionId)
    {
        $userModel = new User();

        if (!$userModel->isExist($userId)) {
            throw new ModelException('Пользователь не найден');
        }

        $isSpeaker = $this->isSpeaker($userId, $sessionId);

        if ($isSpeaker) {
            throw new ModelException('Вы уже записаны');
        }

        if (!$this->attemptToSubscribe($userId, $sessionId)) {
            throw new ModelException('Извините, все места заняты');
        }
    }
}