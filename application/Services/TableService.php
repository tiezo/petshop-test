<?php

namespace Application\Services;


use Framework\Database\Connection;
use Framework\Database\Exceptions\ModelException;
use Framework\Database\Model;
use Framework\Database\Query\QueryBuilder;
use Framework\Support\Collection;
use Framework\Support\Item;

class TableService
{
    public function find($table, $id)
    {
        if (!$this->tableExists($table)) {
            throw new ModelException('Таблица не найдена');
        }

        /** @var Model $className */
        $className = '\\Application\\Models\\' . $table;

        $queryBuilder = new QueryBuilder();
        $queryBuilder
            ->select(['*'])
            ->from($table);

        if (!is_null($id)) {
            $queryBuilder->where($className::$key, '=', $id);
        }

        $result = Connection::performQuery($queryBuilder->build());

        if (!is_null($id)) {
            $data = new Item($result->one());
        } else {
            $data = new Collection($result->all());
        }

        return $this->load($data, $className);
    }

    /**
     * @param $table
     * @return mixed|null
     * @throws ModelException
     */
    public function tableExists($table)
    {
        $query = (new QueryBuilder())
            ->select(['*'])
            ->from('AllowedTables')
            ->where('TableName', '=', $table)
            ->build();

        $result = Connection::performQuery($query)->one();

        return !is_null($result);
    }

    /**
     * @param $data
     * @param $modelClassName
     * @return array
     * @throws ModelException
     */
    public function load($data, $modelClassName)
    {
        /** @var Model $modelClassName */
        $model = new $modelClassName($data);

        /** @var Model $model */
        $model->loadRelations();

        if (!count($model->data->data)) {
            throw new ModelException('Не найдено');
        }

        return $model->data->data;
    }
}