<?php

namespace Application\Controllers;

use Application\Controllers\Traits\JsonApiResponse;
use Framework\Http\Request\Request;

class BaseController
{
    use JsonApiResponse;

    /**
     * @var Request
     */
    protected $request;
}