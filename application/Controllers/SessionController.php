<?php

namespace Application\Controllers;


use Application\Services\SessionService;
use Framework\Database\Exceptions\ModelException;
use Framework\Http\Request\Request;

class SessionController extends BaseController
{
    protected $service;

    public function __construct()
    {
        $this->request = new Request();
        $this->service = new SessionService();
    }

    public function subscribe()
    {
        $sessionId = $this->request->get('sessionId');
        $userId = $this->request->get('userId');

        if (is_null($sessionId) || is_null($userId)) {
            return $this->error('Не заданы необходимые параметры запроса - sessionId и userId');
        }

        try {
            $this->service->subscribe($userId, $sessionId);
            return $this->success([], 'Спасибо, вы успешно записаны!');
        } catch (ModelException $e) {
            return $this->error($e->getMessage());
        }
    }
}