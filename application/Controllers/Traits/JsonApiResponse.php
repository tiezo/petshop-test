<?php

namespace Application\Controllers\Traits;


use Framework\Http\Response\JsonResponse;

trait JsonApiResponse
{
    /**
     * @param $status
     * @param $data
     * @param $message
     * @return JsonResponse
     */
    public function response($status, $data, $message)
    {
        $response = [
            'status' => $status
        ];

        if (!empty($data)) {
            $response['data'] = $data;
        }

        if (mb_strlen($message)) {
            $response['message'] = $message;
        }

        return new JsonResponse($response);
    }

    /**
     * Shortcut for status 'ok'
     *
     * @param array $data
     * @param string $message
     * @return JsonResponse
     */
    public function success($data = [], $message = '')
    {
        return $this->response('ok', $data, $message);
    }

    /**
     * Shortcut for status 'error'
     * @param $message
     * @param array $data
     * @return JsonResponse
     */
    public function error($message, $data = [])
    {
        return $this->response('error', $data, $message);
    }
}