<?php

namespace Application\Controllers;


class IndexController extends BaseController
{
    public function index()
    {
        return $this->success([
            'api' => 'v1'
        ]);
    }
}