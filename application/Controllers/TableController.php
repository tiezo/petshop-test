<?php

namespace Application\Controllers;

use Application\Services\TableService;
use Framework\Database\Exceptions\ModelException;
use Framework\Http\Request\Request;

class TableController extends BaseController
{
    protected $service;

    public function __construct()
    {
        $this->request = new Request();
        $this->service = new TableService();
    }

    public function table()
    {
        $table = $this->request->get('table');
        $id = $this->request->get('id');

        if (is_null($table)) {
            return $this->error('Не был передан необходимый параметр table');
        }

        try {
            $result = $this->service->find($table, $id);

            return $this->success($result);
        } catch (ModelException $e) {
            return $this->error($e->getMessage());
        }
    }
}