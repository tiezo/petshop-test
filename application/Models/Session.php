<?php

namespace Application\Models;


use Framework\Database\Model;
use Framework\Support\ArrayLike;

class Session extends Model
{
    static public $key = 'ID';

    public function __construct(ArrayLike $data = null)
    {
        parent::__construct($data);
        $this->relations['manyToMany'][] = [
            'name' => 'Speakers',
            'mediate' => 'SessionSpeakers',
            'to' => 'User',
            'keys' => ['ID', 'UserId']
        ];
    }


}