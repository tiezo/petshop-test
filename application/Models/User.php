<?php

namespace Application\Models;


use Framework\Database\Connection;
use Framework\Database\Model;
use Framework\Database\Query\QueryBuilder;

class User extends Model
{
    static public $key = 'ID';

    public function isExist($id)
    {
        $query = (new QueryBuilder())
            ->select(['id'])
            ->from('User')
            ->where('id', '=', $id)
            ->build();

        $result = Connection::performQuery($query);

        return !!$result->rowCount;
    }
}